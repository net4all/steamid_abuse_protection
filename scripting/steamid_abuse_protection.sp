#include <sourcemod>
#include <sourcemod>
#include <No_Steam_Info.inc>

#undef REQUIRE_PLUGIN
#include <updater>

#pragma semicolon 1
#pragma newdecls required

#define PLUGIN_VERSION "1.0.5"
#define UPDATE_URL "http://olegtsvetkov.ru/games/sourcemod/steamid_abuse_protection/updatefile.txt"

char g_sLogPath[PLATFORM_MAX_PATH];

public Plugin myinfo =
{
    name = "SteamID Abuse Protection",
    author = "Oleg Tsvetkov",
    description = "",
    version = PLUGIN_VERSION,
    url = "https://net4all.ru/"
};

public void OnPluginStart()
{
    BuildPath(Path_SM, g_sLogPath, sizeof(g_sLogPath), "logs/steamid_abuse_protection.log");
}

public void OnClientPostAdminCheck(int client)
{
    if (IsFakeClient(client) || IsClientSourceTV(client))
    {
        // Skip bots and SourceTV
        return;
    }

    AdminId aid = GetUserAdmin(client);
    if (aid == INVALID_ADMIN_ID)
    {
        // Not an admin
        return;
    }

    if (!GetAdminFlag(aid, Admin_Generic, Access_Effective))
    {
        // Client don't have a "b" flag. Probably VIP or similar.
        return;
    }

    char sClientIP[32];
    GetClientIP(client, sClientIP, sizeof(sClientIP));

    RevEmu_PlayerType playerType = RevEmu_GetPlayerType(client);

    char sPlayerType[64];
    GetRevEmuPlayerTypeAsString(playerType, sPlayerType, sizeof(sPlayerType));

    if (playerType == SteamLegitUser || playerType == Steam2Legit)
    {
        LogToFile(g_sLogPath, "Legit admin connected: %L (IP: %s, Type: %s).", client, sClientIP, sPlayerType);
    }
    else
    {
        LogToFile(g_sLogPath, "Admin's SteamID abuse detected: %L (IP: %s, Type: %s).", client, sClientIP, sPlayerType);

        ServerCommand("sm_banip \"%s\" 0 SteamID abuse", sClientIP);
        KickClient(client, "SteamID abuse");
    }
}

void GetRevEmuPlayerTypeAsString(RevEmu_PlayerType playerType, char[] sBuffer, int size)
{
    if (playerType == ErrorGet)
    {
        strcopy(sBuffer, size, "ErrorGet");
    }
    else if (playerType == SteamLegitUser)
    {
        strcopy(sBuffer, size, "SteamLegitUser");
    }
    else if (playerType == SteamCrackedUser)
    {
        strcopy(sBuffer, size, "SteamCrackedUser");
    }
    else if (playerType == RevEmuUser)
    {
        strcopy(sBuffer, size, "RevEmuUser");
    }
    else if (playerType == RevEmuUserOld)
    {
        strcopy(sBuffer, size, "RevEmuUserOld");
    }
    else if (playerType == SettiSRCScanBot)
    {
        strcopy(sBuffer, size, "SettiSRCScanBot");
    }
    else if (playerType == RevEmuUserV74)
    {
        strcopy(sBuffer, size, "RevEmuUserV74");
    }
    else if (playerType == RevEmuUserVeryOld)
    {
        strcopy(sBuffer, size, "RevEmuUserVeryOld");
    }
    else if (playerType == UnknownUser)
    {
        strcopy(sBuffer, size, "UnknownUser");
    }
    else if (playerType == Steam2Legit)
    {
        strcopy(sBuffer, size, "Steam2Legit");
    }
    else if (playerType == Steam2Cracked)
    {
        strcopy(sBuffer, size, "Steam2Cracked");
    }
    else
    {
        strcopy(sBuffer, size, "Unknown Player Type");
    }
}

// Updater staff
public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
}

public void OnLibraryAdded(const char[] name)
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
}

public int Updater_OnPluginUpdated()
{
    LogMessage("Plugin updated. Old version was %s. Now reloading.", PLUGIN_VERSION);

    ReloadPlugin();
}
